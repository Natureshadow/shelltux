#!/bin/bash

# split and distribute files in multiple directories
mkdir -p chat2
rm -R chat2
mkdir -p chat2
cd chat2

# maximal 10 Zeilen
split -l 10 ../.chat2/beratung.txt

mkdir _x
mv x* _x
mv _x x
cd x
mkdir a
mv xa* a
mkdir b
mv xb* b
cd ../../..
