README
======

SITUATION
---------

Im Verzeichnis 'tech-info' befinden sich mehrere Dateien.

Die Dateien sind zu groß, als dass man sie mit cat über
eine Bildschirmseite ansehen kann.

Um die Dateien trotzdem vollständig sehen zu können, gibt es
den Befehl

    less DATEI

Nun kannst du mit den Cursor-Tasten oder den Bild-Rauf/Runter-
Tasten durch die Datei scrollen.
Mit der Taste q kann man less wieder verlassen.

Weiter mit README2.md.

P. S.:
Das erste Programm dieser Art hieß "more" (englisch für "mehr").
Der verbesserte Nachfolger wurde "less" (englisch für "weniger") genannt.
Die UNIX-Entwickler hatten schon immer Sinn für Humor. :-)
Siehe auch https://de.wikipedia.org/wiki/Less_(Unix).
