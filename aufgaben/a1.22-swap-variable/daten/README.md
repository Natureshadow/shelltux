Variablen vertauschen
=====================

Schau dir das Script swap.sh an.

1. Lasse es einmal laufen und schaue was es macht.

2. Ändere die Stelle # "Dein Code hier..." so ab, dass die Variablen
   A und B vertauscht werden.

3. Prüfe deine Änderungen, indem du das Script ausführst.

Dann weiter mit shelltux solve.

