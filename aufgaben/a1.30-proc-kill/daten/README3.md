Einen Prozess beenden
=====================

Mit dem Befehl

    kill PID

kann man einen Prozess beenden, wenn man seine PID kennt.

Nun beende den Mr.-X-Prozess, damit die Ausgabe der Zahlen
ein Ende hat.

Weiter mit shelltux solve
