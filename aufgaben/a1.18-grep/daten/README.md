grep
====

Suche in der Datei 'wiki-artikel-1' nach dem Wort "Mr. X"
und finde dann heraus, in welcher Zeile er sich befindet.

Die Zeilennummer für die Lösung merken.

Die nächste Aufgabe befindet sich in der README-2.md.

