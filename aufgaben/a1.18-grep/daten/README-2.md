grep -n
=======

Für die Datei 'wiki-artikel-2' finde erst heraus, wie oft
die Zeichenkette 'meist' darin vorkommt.

Danach finde heraus, in welchen Zeilen die Zeichenkette vorkommt.

Dazu kannst du die Option -n von grep verwenden:

    grep -n     - zeigt Zeilennummern

Addiere die Zeilennummern zu der Zahl aus der Aufgabe aus der
README.md.

Die Summe ist die Lösung.

