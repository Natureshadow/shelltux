Hinweis
=======
Leider hat Mr. X ein Programm gestartet, das das Spiel
nach einiger Zeit mit einem fiesen Mr. X-Hinweis abbricht.

Deine Aufgabe ist es...

1) herauszufinden, nach welcher Zeit in Sekunden das Spiel unterbrochen wird.

dazu kannst du entweder die Zeit mit einer Methode deine Wahl messen
oder den time-Befehl verwenden

    time moon-buggy

gibt drei Zeilen aus. Die erste Zeile sieht z. B. so aus: "real    10m15,793"
Das heißt, der Prozess lief 10 Minuten, 15 Sekunden und 793 Tausendstel Sekunden.

2) in der README-v2.md nachzulesen wie man das Programm stoppen kann.

