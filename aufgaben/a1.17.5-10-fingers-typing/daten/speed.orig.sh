#!/bin/sh
# speed.sh: a very tiny utility to measure typing speed.
# from: https://www.linuxjournal.com/content/how-fast-can-you-type-develop-tiny-utility-bash-find-out, 2011
# 2018: some minor fixes
prompt="Start typing a piece of text. Press Ctrl-D to finish."
echo -e "\n$prompt \n"
start_time=`date +%s`
words=`cat|wc -w`
end_time=`date +%s`
speed=`echo "scale=2; $words / ( ( $end_time - $start_time ) / 60)" | bc`
echo -e "\n\nYou have a typing speed of $speed words per minute."
