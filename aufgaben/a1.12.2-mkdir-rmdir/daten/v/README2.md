Aufgaben zu Verzeichnissen (2)
==============================

Aufgabe 3
---------
Mit dem Befehl

    rmdir VERZEICHNIS

kann man ein leeres Verzeichnis löschen.

rmdir steht für englisch "remove directory" (= "Verzeichnis entfernen").
                          ^ ^    ^^^
Verwende rmdir und rm, um das Verzeichnis `m` und alles,
was darin ist, zu löschen.
Beachte, dass ein Verzeichnis erst leer sein muss,
bevor du es mit rmdir löschen kannst.

Weiter mit README3.md...
