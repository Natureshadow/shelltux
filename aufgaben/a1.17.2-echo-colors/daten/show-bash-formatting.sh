#!/bin/bash

# The base for this script was this great tutorial:
#   https://misc.flogisoft.com/bash/tip_colors_and_formatting

# TODO:
#   - use printf for the spaces formatting, see flogi-256-colors.sh
#   - (Mit Python ncurses?) die 88/256 Colors ausgeben.

print_formatting_1() {
    # $1
    A=("$@") # https://askubuntu.com/questions/674333/how-to-pass-an-array-as-function-argument
    # echo ${A[@]}

    CODE=${A[0]}
    DESC=${A[1]}
    EXAMPLE=${A[2]}

    echo -n "$CODE    | $DESC | $EXAMPLE --> "
    eval $EXAMPLE
}

print_formatting_2() {
    A=("$@")
    CODE=${A[0]}
    DESC=${A[1]}
    SPACE=${A[2]}
    EXAMPLE="echo -e \"Farbe \e[${CODE}m${DESC}\e[0m.\"$SPACE"
    echo -n "$CODE | $DESC$SPACE | $EXAMPLE --> "
    eval $EXAMPLE
}

print_formatting_3() {
    A=("$@")
    CODE=${A[0]}
    SPACE1=${A[1]}
    DESC=${A[2]}
    SPACE2=${A[3]}
    EXAMPLE="echo -e \"\e[${CODE}m${DESC}\e[0m.\"$SPACE"
    echo "$CODE$SPACE1 | $EXAMPLE"
    echo -n "              "
    eval $EXAMPLE
}


echo "Farblose Formatierung"
echo "---------------------"
echo "CODE | Beschreibung    | Beispiel"
echo "--------------------------------------------------------------------"
A0=(1 "Fett/Hell      " "echo -e \"Hallo \e[1mFett\e[0m.\"           ")
A1=(2 "Gedimmt        " "echo -e \"Hallo \e[2mGedimmt\e[0m.\"        ")
A2=(4 "Unterstrichen  " "echo -e \"Hallo \e[4mUnterstrichen\e[0m.\"  ")
A3=(5 "Blinken        " "echo -e \"Hallo \e[5mBlinken\e[0m.\"        ")
A4=(7 "Invertiert     " "echo -e \"Hallo \e[7mInvertiert\e[0m.\"     ")
A5=(8 "Versteckt      " "echo -e \"Hallo \e[8mPasswort\e[0m.\"       ")
print_formatting_1 "${A0[@]}"
print_formatting_1 "${A1[@]}"
print_formatting_1 "${A2[@]}"
print_formatting_1 "${A3[@]}"
print_formatting_1 "${A4[@]}"
print_formatting_1 "${A5[@]}"

echo
echo "Farben im Vordergrund"
echo "---------------------"

A00=(39 "Standard" "   ")
A01=(30 "Schwarz" "    ")
A02=(31 "Rot" "        ")
A03=(32 "Grün" "       ")
A04=(33 "Gelb" "       ")
A05=(34 "Blau" "       ")
A06=(35 "Magenta" "    ")
A07=(36 "Cyan" "       ")
A08=(37 "Hellgrau" "   ")
A09=(90 "Dunkelgrau" " ")
A10=(91 "Hellrot" "    ")
A11=(92 "Hellgrün" "   ")
A12=(93 "Hellgelb" "   ")
A13=(94 "Hellblau" "   ")
A14=(95 "Hellmagenta" "")
A15=(96 "Hellcyan" "   ")
A16=(97 "Weiß" "       ")
print_formatting_2 "${A00[@]}"
print_formatting_2 "${A01[@]}"
print_formatting_2 "${A02[@]}"
print_formatting_2 "${A03[@]}"
print_formatting_2 "${A04[@]}"
print_formatting_2 "${A05[@]}"
print_formatting_2 "${A06[@]}"
print_formatting_2 "${A07[@]}"
print_formatting_2 "${A08[@]}"
print_formatting_2 "${A09[@]}"
print_formatting_2 "${A10[@]}"
print_formatting_2 "${A11[@]}"
print_formatting_2 "${A12[@]}"
print_formatting_2 "${A13[@]}"
print_formatting_2 "${A14[@]}"
print_formatting_2 "${A15[@]}"
print_formatting_2 "${A16[@]}"

echo
echo "Farben im Hintergrund"
echo "---------------------"
A00=("49" "Standard" "    ")
A01=("40" "Schwarz" "     ")
A02=("41" "Rot" "         ")
A03=("42" "Grün" "        ")
A04=("43" "Gelb" "        ")
A05=("44" "Blau" "        ")
A06=("45" "Magenta" "     ")
A07=("46" "Cyan" "        ")
A08=("47" "Hellgrau" "    ")
A09=("100" "Dunkelgrau" " ")
A10=("101" "Hellrot" "    ")
A11=("102" "Hellgrün" "   ")
A12=("103" "Hellgelb" "   ")
A13=("104" "Hellblau" "   ")
A14=("105" "Hellmagenta" "")
A15=("106" "Hellcyan" "   ")
A16=("107" "Weiß" "       ")
print_formatting_2 "${A00[@]}"
print_formatting_2 "${A01[@]}"
print_formatting_2 "${A02[@]}"
print_formatting_2 "${A03[@]}"
print_formatting_2 "${A04[@]}"
print_formatting_2 "${A05[@]}"
print_formatting_2 "${A06[@]}"
print_formatting_2 "${A07[@]}"
print_formatting_2 "${A08[@]}"
print_formatting_2 "${A09[@]}"
print_formatting_2 "${A10[@]}"
print_formatting_2 "${A11[@]}"
print_formatting_2 "${A12[@]}"
print_formatting_2 "${A13[@]}"
print_formatting_2 "${A14[@]}"
print_formatting_2 "${A15[@]}"
print_formatting_2 "${A16[@]}"

echo
echo "Kombinationen"
echo "-------------"
echo "Indem man Codes mit Semikolon (;) abtrennt, kann man sie kombinieren. Beispiele:"
echo
A00=("1;4" "        " "Fett und unterschrichen" "                           ")
A01=("1;33;44" "    " "Fett und gelbe Schrift und blauer Hintergrund" "     ")
A02=("1;4;33;100" " " "Fett, unterschrichen, gelb und dunkelgrau" "         ")
print_formatting_3 "${A00[@]}"
print_formatting_3 "${A01[@]}"
print_formatting_3 "${A02[@]}"
