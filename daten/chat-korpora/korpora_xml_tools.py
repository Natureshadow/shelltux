#!/usr/bin/python3

import sys
# https://docs.python.org/3/library/xml.etree.elementtree.html
import xml.etree.ElementTree as ET

def make_simple_text_file(input_file, out_file):
    """
    Nimmt eine Korpora-XML-Datei und konvertiert sie in eine einfache Text-Datei
    mit Zeilen:
        <USER>: <MESSAGE>
    """
    # print("Hallo {0}".format(input_file))

    tree = ET.parse(input_file)
    root = tree.getroot()
    body = root.find('body')

    with open(out_file, 'w') as f:
        for message in body:
            nickname = message.find('messageHead').find('nickname').text.strip()
            messageBody = message.find('messageBody')
            message = messageBody.text
            # print(message)
            for child in messageBody:
                message = "{0}{1}{2}".format(message, child.text, child.tail)
                #print("  ", child.tag, child.attrib, child.tail)
            message = message.strip()
            f.write("{0}: {1}\n".format(nickname, message))

def main():
    if len(sys.argv) < 2:
        print("Usage:")
        print("    $ korpora_xml_tools.py mstf_v1 FILE_IN FILE_OUT - convert the given XML file to text file")
        exit(0)

    p1 = sys.argv[1];
    if p1 == "mstf_v1":
        p2 = sys.argv[2];
        p3 = sys.argv[3];
        make_simple_text_file(p2, p3)

if __name__ == "__main__":
    # execute only if run as a script
    main()


"""
Beispiele:
./korpora_xml_tools.py mstf_v1 1202016_RUB_Beratung_nachSystemcrash_17-06-2004.xml a.txt
./korpora_xml_tools.py mstf_v1 1306016_bluewin_Thomas_Bucheli_28-10.xml b.txt
"""
