ShellTux - Das interaktive Bash-Tutorium
========================================

![](img/st-0.png)

Spielerisch die Bash-Kommandozeile kennenlernen und dabei auch etwas über GNU/Linux-Systeme und Programmieren lernen.

Lizensiert unter der GNU AGPLv3, 2017 - 2018

Schnellnavigation:

* [Installation / Tutor](stREADME-inst.md)
* [Fehler gefunden? / Entwickler / Beitragen](stREADME-dev.md)

Inhalt:

<!-- toc -->

- [Zielgruppe](#zielgruppe)
- [Besondere Merkmale](#besondere-merkmale)
- [Screenshots](#screenshots)

<!-- tocstop -->

Zielgruppe
----------
Zielgruppe sind Schülerinnen und Schüler der Jahrgangsstufen 5 - 13 für einen spielerischen Einstieg in folgende Themen:

- Bash-Shell
    - Grundlegende Bedienung (keine Vorkenntnisse erforderlich)
    - ls, cd
    - Farben
- Dateien und Verzeichnisse
    - Anzeigen
    - Bearbeiten
    - Löschen
    - Platzhalter
    - Suchen
    - Kopieren, Verschieben
- Fortgeschrittene Themen
    - Umleitungen
    - Eigene Scripte
    - Dateiattribute
- Prozesse
    - Finden
    - Beenden
- Programmieren
    - Shell-Programmierung
    - Variablen
    - Reguläre Ausdrücke (in Planung)

Besondere Merkmale
------------------
- "Offline first"
    - Das Programm kann ohne Internetverbindung verwendet werden (außer Aufgaben erfordern Internet)
    - Kann im Klassennetzwerk per SSH verwendet werden
- Alles wird mit Freier Software umgesetzt
- Fokus auf Aktivität mit dem Computer (eher wenig Texte lesen / "Learning by Doing")
    - Je nach Aufgabe werden auch Hintergrundprozesse gestartet, um eine Dynamik reinzubringen
- Eigenständigkeit, reduzierter Betreungsaufwand
    - Die Aufgaben sind interaktiv und enden oft in einem automatischen Test
- Reproduzierbare Umgebung
    - Das Arbeitsverzeichnis wird für jede Übung neu aufgebaut
- Motivation: Die Einstigsaufgaben sind in eine kleine Geschichte um einen mysteriösen Mr. X eingebettet
- Details
    - Es wird schrittweise ein Merkblatt mit den wichtigsten Befehlen erarbeitet
    - Der Fortschritt wird gespeichert, so dass man beim nächsten Mal fortsetzen kann
    - ShellTux ist selber ein Bash-Script und alle Inhalte werden über Kommandozeile vermittelt
- Modularer Aufbau
    - Weitere Aufgaben können einfach hinzugefügt werden
    - Abhängigkeiten zwischen Aufgaben können definiert werden
- Primärer Sprachraum ist Deutsch; Übersetzungen willkommen

Screenshots
-----------
Starten mit `shelltux`:

![](img/st-1-start.png)

Einleitung:

![](img/st-2-intro.png)

Nächste Aufgabe starten oder neu starten (wenn noch nicht gelöst):

![](img/st-3-next-task.png)

Aufgabe 'Dateien auflisten mit ls':

![](img/st-4-ls.png)

Lösung einreichen mit `shelltux solve`:

![](img/st-5-solve.png)

Aufgabe 'Verzeichnis wechseln mit cd':

![](img/st-6-cd.png)

Aufgabe 'cd ..' mit virtuellem Tutor:

![](img/st-7-demo.gif)

Erklärung der ls-Farben:

![](img/st-7a-farben.png)

Bash-Formatierungsreferenz:

![](img/st-7b-farben.png)

Detektiv-Aufgabe:

![](img/st-8-rm.png)

Status anzeigen mit `shelltux status`:

![](img/st-9-status.png)

Anhängigkeitsgraph der Aufgaben (Stand April 2018):

![](img/st-10-depenceny.png)

Hintergrundwissen einstreuen:

![](img/st-11-less.png)

Befehlsreferenz von bereits gelösten Aufgaben:

![](img/st-12-merkblatt.png)
